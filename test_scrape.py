import scrape
import pytest
import pandas as pd


@pytest.fixture
def valid_url():
    return 'https://www.google.com'


@pytest.fixture
def bad_url():
    return 'https://www.daoiwghruoa.com'


def test_simple_get(valid_url, bad_url):
    t1 = scrape.simple_get(valid_url)
    assert t1 is not None
    assert isinstance(t1, bytes)
    t2 = scrape.simple_get(bad_url)
    assert t2 is None


def test_get_listings_from_carsdotcom():
    test_result = scrape.get_listings_from_carsdotcom(60615, 50, 5, 100)
    assert isinstance(test_result, type(pd.DataFrame()))
    assert len(test_result) == 100 * 5

